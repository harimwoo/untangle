# Generated by Django 4.1.7 on 2023-02-27 20:01

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Log',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('mood', models.PositiveSmallIntegerField(choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10)])),
                ('people_seen', models.TextField()),
                ('morning_activities', models.TextField()),
                ('afternoon_activities', models.TextField()),
                ('evening_activities', models.TextField()),
                ('reflection', models.TextField()),
                ('grateful', models.TextField()),
            ],
        ),
    ]
