from django.shortcuts import render, get_object_or_404, redirect
from log.models import Log
from log.forms import LogForm

def list_log(request):
    logs = Log.objects.order_by('-date')
    context = {
        "logs": logs
    }
    return render(request, "logs/list.html", context)

def show_log(request, id):
    log = get_object_or_404(Log, id=id)
    context = {
        "log_object": log
    }
    return render(request, "logs/detail.html", context)

def create_log(request):
    if request.method == "POST":
        form = LogForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(list_log)
    else:
        form = LogForm()
        context = {
            "form": form,
        }
    return render(request, "logs/create.html", context)

def edit_log(request, id):
    log = get_object_or_404(Log, id=id)
    if request.method == "POST":
        form = LogForm(request.POST, instance=log)
        if form.is_valid():
            form.save()
            return redirect('show_log', id=id)
    else:
        form = LogForm(instance=log)
    context = {
            "log_object": log,
            "log_form": form
        }
    return render(request, "logs/edit.html", context)
