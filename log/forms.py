from django.forms import ModelForm
from django import forms
from log.models import Log

class LogForm(ModelForm):
    class Meta:
        model = Log
        fields = [
            "date",
            "mood",
            "positive_habit",
            "negative_habit",
            "morning_activities",
            "afternoon_activities",
            "evening_activities",
            "reflection",
            "grateful"
        ]
