from django.urls import path

from log.views import list_log, show_log, create_log, edit_log

urlpatterns = [
    path("", list_log, name="list_log"),
    path("<int:id>/", show_log, name="show_log"),
    path("create/", create_log, name='create_log'),
    path("<int:id>/edit/", edit_log, name="edit_log"),
]
