from django.db import models
# Create your models here.
class Log(models.Model):
    date = models.DateField()
    mood = models.PositiveSmallIntegerField(choices=zip(range(1,11), range(1, 11)))
    people_seen = models.TextField(null=True)
    positive_habit = models.TextField(null=True)
    negative_habit = models.TextField(null=True)
    morning_activities = models.TextField(null=True)
    afternoon_activities = models.TextField(null=True)
    evening_activities = models.TextField(null=True)
    reflection = models.TextField(null=True)
    grateful = models.TextField(null=True)

    def __str__(self):
        return f"Log for {self.date}"
